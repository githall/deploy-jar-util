﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DeployJarUtil
{
    public partial class LogForm : Form
    {
        System.Windows.Forms.Timer timer;
        public LogForm()
        {
            InitializeComponent();
        }

        private void LogForm_Load(object sender, EventArgs e)
        {
            timer = new System.Windows.Forms.Timer();
            //1秒间隔
            timer.Interval = 1000;
            //执行事件
            timer.Tick += (s, e1) =>
            {
                RefreshLogContent();
            };
            //开始执行
            timer.Start();
        }


        public void RefreshLogContent()
        {
            string logPath = AppDomain.CurrentDomain.BaseDirectory + "deploylog\\deploy.out.log";
            string logContent = ReadFileContent(logPath);
            SetTextCallback d = new SetTextCallback(SetText);
            this.txt_Log.Invoke(d, new object[] { logContent });
        }

        public string ReadFileContent(string FileFullName)
        {
            if (File.Exists(FileFullName))
            {
                System.IO.FileStream fs = new System.IO.FileStream(FileFullName, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                string FileContent = "";
                try
                {
                    int fsLen = Convert.ToInt32(fs.Length);
                    byte[] heByte = new byte[fsLen];
                    int r = fs.Read(heByte, 0, heByte.Length);
                    FileContent = System.Text.Encoding.Default.GetString(heByte);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    fs.Close();
                    fs.Dispose();
                }
                return FileContent;
            }
            else
            {
                return "";
            }

        }

        delegate void SetTextCallback(string text);

        private void SetText(string text)
        {
            txt_Log.Text = "";
            txt_Log.AppendText(text);
        }

        private void LogForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer.Stop();
        }

    }
}
