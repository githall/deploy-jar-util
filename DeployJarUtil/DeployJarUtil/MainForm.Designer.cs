﻿namespace DeployJarUtil
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ServerName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_JarPath = new System.Windows.Forms.TextBox();
            this.btn_SelectJarPath = new System.Windows.Forms.Button();
            this.btn_InstallService = new System.Windows.Forms.Button();
            this.btn_UnstallService = new System.Windows.Forms.Button();
            this.btn_StartService = new System.Windows.Forms.Button();
            this.btn_StopService = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Port = new System.Windows.Forms.TextBox();
            this.btn_SaveConfig = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Result = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务名称：";
            // 
            // txt_ServerName
            // 
            this.txt_ServerName.Location = new System.Drawing.Point(118, 26);
            this.txt_ServerName.Name = "txt_ServerName";
            this.txt_ServerName.Size = new System.Drawing.Size(288, 21);
            this.txt_ServerName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "jar包路径：";
            // 
            // txt_JarPath
            // 
            this.txt_JarPath.Location = new System.Drawing.Point(118, 63);
            this.txt_JarPath.Name = "txt_JarPath";
            this.txt_JarPath.ReadOnly = true;
            this.txt_JarPath.Size = new System.Drawing.Size(288, 21);
            this.txt_JarPath.TabIndex = 3;
            // 
            // btn_SelectJarPath
            // 
            this.btn_SelectJarPath.Location = new System.Drawing.Point(412, 63);
            this.btn_SelectJarPath.Name = "btn_SelectJarPath";
            this.btn_SelectJarPath.Size = new System.Drawing.Size(75, 23);
            this.btn_SelectJarPath.TabIndex = 4;
            this.btn_SelectJarPath.Text = "选择";
            this.btn_SelectJarPath.UseVisualStyleBackColor = true;
            this.btn_SelectJarPath.Click += new System.EventHandler(this.btn_SelectJarPath_Click);
            // 
            // btn_InstallService
            // 
            this.btn_InstallService.Enabled = false;
            this.btn_InstallService.Location = new System.Drawing.Point(130, 291);
            this.btn_InstallService.Name = "btn_InstallService";
            this.btn_InstallService.Size = new System.Drawing.Size(84, 33);
            this.btn_InstallService.TabIndex = 8;
            this.btn_InstallService.Text = "安装服务";
            this.btn_InstallService.UseVisualStyleBackColor = true;
            this.btn_InstallService.Click += new System.EventHandler(this.btn_InstallService_Click);
            // 
            // btn_UnstallService
            // 
            this.btn_UnstallService.Enabled = false;
            this.btn_UnstallService.Location = new System.Drawing.Point(219, 291);
            this.btn_UnstallService.Name = "btn_UnstallService";
            this.btn_UnstallService.Size = new System.Drawing.Size(84, 33);
            this.btn_UnstallService.TabIndex = 9;
            this.btn_UnstallService.Text = "卸载服务";
            this.btn_UnstallService.UseVisualStyleBackColor = true;
            this.btn_UnstallService.Click += new System.EventHandler(this.btn_UnstallService_Click);
            // 
            // btn_StartService
            // 
            this.btn_StartService.Enabled = false;
            this.btn_StartService.Location = new System.Drawing.Point(307, 291);
            this.btn_StartService.Name = "btn_StartService";
            this.btn_StartService.Size = new System.Drawing.Size(84, 33);
            this.btn_StartService.TabIndex = 10;
            this.btn_StartService.Text = "启动服务";
            this.btn_StartService.UseVisualStyleBackColor = true;
            this.btn_StartService.Click += new System.EventHandler(this.btn_StartService_Click);
            // 
            // btn_StopService
            // 
            this.btn_StopService.Enabled = false;
            this.btn_StopService.Location = new System.Drawing.Point(396, 291);
            this.btn_StopService.Name = "btn_StopService";
            this.btn_StopService.Size = new System.Drawing.Size(84, 33);
            this.btn_StopService.TabIndex = 11;
            this.btn_StopService.Text = "停止服务";
            this.btn_StopService.UseVisualStyleBackColor = true;
            this.btn_StopService.Click += new System.EventHandler(this.btn_StopService_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "部署端口：";
            // 
            // txt_Port
            // 
            this.txt_Port.Location = new System.Drawing.Point(118, 98);
            this.txt_Port.Name = "txt_Port";
            this.txt_Port.Size = new System.Drawing.Size(288, 21);
            this.txt_Port.TabIndex = 13;
            // 
            // btn_SaveConfig
            // 
            this.btn_SaveConfig.Location = new System.Drawing.Point(43, 291);
            this.btn_SaveConfig.Name = "btn_SaveConfig";
            this.btn_SaveConfig.Size = new System.Drawing.Size(84, 33);
            this.btn_SaveConfig.TabIndex = 14;
            this.btn_SaveConfig.Text = "保存配置";
            this.btn_SaveConfig.UseVisualStyleBackColor = true;
            this.btn_SaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "执行结果：";
            // 
            // txt_Result
            // 
            this.txt_Result.Location = new System.Drawing.Point(118, 138);
            this.txt_Result.Multiline = true;
            this.txt_Result.Name = "txt_Result";
            this.txt_Result.Size = new System.Drawing.Size(288, 133);
            this.txt_Result.TabIndex = 16;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 349);
            this.Controls.Add(this.txt_Result);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_SaveConfig);
            this.Controls.Add(this.txt_Port);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_StopService);
            this.Controls.Add(this.btn_StartService);
            this.Controls.Add(this.btn_UnstallService);
            this.Controls.Add(this.btn_InstallService);
            this.Controls.Add(this.btn_SelectJarPath);
            this.Controls.Add(this.txt_JarPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_ServerName);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jar包部署工具";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_ServerName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_JarPath;
        private System.Windows.Forms.Button btn_SelectJarPath;
        private System.Windows.Forms.Button btn_InstallService;
        private System.Windows.Forms.Button btn_UnstallService;
        private System.Windows.Forms.Button btn_StartService;
        private System.Windows.Forms.Button btn_StopService;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Port;
        private System.Windows.Forms.Button btn_SaveConfig;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Result;
    }
}

